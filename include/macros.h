// MAI Cup 2022 Senior Team
// Copyright (C) 2021  MAI Robotics
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MACROS_H
#define MACROS_H

#include <Arduino.h>

#include "config.h"

#if DEBUG
#define DEBUG_PRINT(x) Serial.print(x)
#define DEBUG_PRINTLN(x) Serial.println(x)

#define DEBUG_PRINT_F(x) DEBUG_PRINT(F(x))
#define DEBUG_PRINTLN_F(x) DEBUG_PRINTLN(F(x))

#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINT_F(x)

#define DEBUG_PRINTLN(x)
#define DEBUG_PRINTLN_F(x)
#endif

#define FORCE_INLINE  __attribute__((always_inline)) inline
#define NO_INLINE      __attribute__((noinline))

// Pragma defines for GCC and CLang //
#define DO_PRAGMA(X) _Pragma(#X)
#define DISABLE_WARNING_PUSH DO_PRAGMA(GCC diagnostic push)
#define DISABLE_WARNING(warningName) \
    DO_PRAGMA(GCC diagnostic ignored #warningName)
#define DISABLE_WARNING_POP DO_PRAGMA(GCC diagnostic pop)

///Convert a pin on a port to a bitmask
#define port_pin_to_bitmask(p) (1<<(p))
/// Set Pin
#define SET(port_register, pin) ((port_register) |= port_pin_to_bitmask(pin))
/// Clear Pin
#define CLR(port_register, pin) ((port_register) &= ~port_pin_to_bitmask(pin))
/// Invert Bit via the PIN (Port Input) Register
#define SBI(pin_register, pin) ((pin_register) |= port_pin_to_bitmask(pin))
/// Toggle Pin using XOR on Port Register
#define TOG(port_register, pin) ((port_register) ^= port_pin_to_bitmask(pin))

#endif //MACROS_H
