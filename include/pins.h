// MAI Cup 2022 Senior Team
// Copyright (C) 2021  MAI Robotics
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef PINS_H
#define PINS_H

#include "macros.h"

#define STEP_LEFT       A6
#define ENABLE_LEFT     A2
#define DIR_LEFT        A7

#define STEP_RIGHT      A0
#define ENABLE_RIGHT    38
#define DIR_RIGHT       A1

#define STEP_LEFT_PORT      PORTF
#define STEP_LEFT_PIN_REG   PINF
#define STEP_LEFT_PIN       6

#define ENABLE_LEFT_PORT    PORTF
#define ENABLE_LEFT_PIN_REG PINF
#define ENABLE_LEFT_PIN     2

#define DIR_LEFT_PORT       PORTF
#define DIR_LEFT_PIN_REG    PINF
#define DIR_LEFT_PIN        7

#define STEP_RIGHT_PORT     PORTF
#define STEP_RIGHT_PIN_REG  PINF
#define STEP_RIGHT_PIN      0

#define ENABLE_RIGHT_PORT   PORTD
#define ENABLE_RIGHT_PIN_REG    PIND
#define ENABLE_RIGHT_PIN    7

#define DIR_RIGHT_PORT      PORTF
#define DIR_RIGHT_PIN_REG   PINF
#define DIR_RIGHT_PIN       1

#define LED_BUILTIN_PORT    PORTB
#define LED_BUILTIN_PIN_REG PINB
#define LED_BUILTIN_PIN     7

#endif //PINS_H
